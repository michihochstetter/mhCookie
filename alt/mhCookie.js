/*! mhCookie v3.0 | (c) Michael Hochstetter | LinkToGitlabMITLicense !*/
(function($){
	$.mhCookie = function(options) {
		// --- START
		// instance
		var instance = this;

		// settings
		var _settings = $.extend({
			link: "",
			linkText: "&raquo; Datenschutzerklärung",
			cookieText: "Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren, Funktionen für soziale Medien anbieten zu können und die Zugriffe auf unsere Webseite zu analysieren. Außerdem geben wir Informationen zu Ihrer Verwendung unserer Webseite an unsere Partner für soziale Medien, Werbung und Analysen weiter. Unsere Partner führen diese Informationen möglicherweise mit weiteren Daten zusammen, die Sie ihnen bereitgestellt haben oder die Sie im Rahmen Ihrer Nutzung der Dienste gesammelt haben.",
			btnAgreeText: "akzeptieren",
			btnDenyText: "ablehnen",
			fadeTime: 300,
			isAllowed: function(){},
			isDenied: function(){},
			isUnknown: function(){}
		}, options || {});

		// variables
		var cookieName = "mhCookie_agreement";

		// functions
		function createStructur() {
			if($('#mhCookie_container').length!==1) {
				$('body').append("<div id='mhCookie_container'>");
					$('#mhCookie_container').append("<div id='mhCookie_wrapper'>");
						$('#mhCookie_wrapper').append("<div id='mhCookie_close'>x</div>");
						$('#mhCookie_wrapper').append("<div id='mhCookie_text'>" + _settings.cookieText + "</div>");
						$('#mhCookie_wrapper').append("<div id='mhCookie_dslink'>");
							$('#mhCookie_dslink').append("<a href='" + _settings.link + "'>" + _settings.linkText + "</a>");
						$('#mhCookie_wrapper').append("<div id='mhCookie_control'>");
							$('#mhCookie_control').append("<a id='mhCookie_btnAgree' class='mhCookie_btn agree" + (getCookie(cookieName)==='agree' ? " active" : "") + "'>" + _settings.btnAgreeText + "</a>");
							$('#mhCookie_control').append("<a id='mhCookie_btnDeny' class='mhCookie_btn deny" + (getCookie(cookieName)==='deny' ? " active" : "") + "'>" + _settings.btnDenyText + "</a>");
			}
		}
		function init() {
			if(_settings.link!="") {
				createStructur();

				if(getCookie(cookieName)==='agree' || getCookie(cookieName)==='deny') {
					if(getCookie(cookieName)==='agree') {
						_settings.isAllowed();
					} else if(getCookie(cookieName)==='deny') {
						_settings.isDenied();
					}
					return;
				} else {
					show();
				}
			} else {
				console.error("mhCookie: Es muss ein Link zur Datenschutz-Seite angegeben werden!");
			}
			_settings.isUnknown();
		}
		function show() {
			$('#mhCookie_container').fadeIn(_settings.fadeTime);
		}
		function hide() {
			$('#mhCookie_container').fadeOut(_settings.fadeTime);
		}
		function agree(status) {
			if(status===true) {
				setCookie(cookieName, "agree");
			} else {
				setCookie(cookieName, "deny");
			}
		}

		// bind events
		$(document).on('click','#mhCookie_btnAgree:not(.active)',function(e){
			e.preventDefault();
			agree(true);
			location.reload();
			return false;
		});
		$(document).on('click','#mhCookie_btnDeny:not(.active)',function(e){
			e.preventDefault();
			agree(false);
			location.reload();
			return false;
		});
		$(document).on('click','#mhCookie_btnSettings',function(e){
			e.preventDefault();
			show();
			return false;
		});

		// start the plugin
		init();

		// self retun
		return instance;
		// --- ENDE
	};

	function setCookie(name,value,days) {
		var expires = "";
		var date = new Date();
		if (days) {
			date.setTime(date.getTime() + (days*24*60*60*1000));
		} else {
			date.setTime(date.getTime() + (365*24*60*60*1000)) // 1 year
		}
		expires = "; expires=" + date.toUTCString();

		document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	}
	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
		    var c = ca[i];
		    while (c.charAt(0)==' ') c = c.substring(1, c.length);
		    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	function eraseCookie(name) {   
		setCookie(name,"",0);
	}

}(jQuery));
