# Ein neues Update wird bearbeitet

# mhCookie
jQuery Cookie Opt-In für Webseiten

# Installation / Usage
1. Download der Dateien mhCookie.js und mhCookie.css
2. Einbindung der CSS-Datei in den Head-Bereich:
```html
<link rel="stylesheet" href="mhCookie.css">
```
3. Einbindung der JS-Datei am Ende des Bodys:
```html
<script src="mhCookie.js"></script>
```
4. Einstellungen anpassen
```html
$.mhCookie({
	link: "", // required !
	linkText: "Link zur Datenschutzerklärung",
	cookieText: "Wir verwenden Cookies, um Inhalte und Anzeigen zu personalisieren, Funktionen für soziale Medien anbieten zu können und die Zugriffe auf unsere Webseite zu analysieren. Außerdem geben wir Informationen zu Ihrer Verwendung unserer Webseite an unsere Partner für soziale Medien, Werbung und Analysen weiter. Unsere Partner führen diese Informationen möglicherweise mit weiteren Daten zusammen, die Sie ihnen bereitgestellt haben oder die Sie im Rahmen Ihrer Nutzung der Dienste gesammelt haben.",
	btnAgreeText: "akzeptieren",
	btnDenyText: "ablehnen",
	fadeTime: 300,
	isAllowed: function() {

		// Code for allowed Cookie-Usage

	},
	isDenied: function() {

		// Code for denied Cookie-Usage

	},
	isUnknown: function() {

		// Default if User isn't known

	}
});
```
5. Einbau des Einstellungsbuttons zum erneuten öffnen von mhCookie
```html
<a href="#" id="mhCookie_btnSettings">Cookie-Einstellungen</a>
```
