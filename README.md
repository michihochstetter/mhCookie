# mhCookie
jQuery Cookie Opt-In für Webseiten

# Screenshot
![screenshot mhCookie](./screenshot.jpg)

# Installation / Usage
1. Download der Dateien mhCookie.js und mhCookie.css
2. Einbindung der CSS-Datei und der JS-Dateien in den Head-Bereich:
```html
<link rel="stylesheet" type="text/css" href="/css/mhCookie.min.css">
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/mhCookie.min.js" data-seite="{Link zur Datenschutz Seite}" data-tools="googleMaps,googleAnalytics,googleAdsense,googleRecaptcha" id="mhCookie_script"></script>
```
Folgende Tools werden derzeit unterstützt (data-tools):
- googleMaps (Google-Maps)
- googleAnalytics (Google-Analytics)
- googleAdsense (Google-Adsense)
- googleRecaptcha (Google Recaptcha)
3. a) Einbindung Google-Maps
```html
<iframe mhCookie-tool="googleMaps" data-src="{Google Maps Teilen Link}" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
```
3. b) Einbindung Google-Maps mittels javascript
```html
<div mhCookie-tool="googleMaps" id="googleMapsMap" callback="mapsInitiate" lat="{LATITUDE}" lng="{LONGITUDE}" zoom="{ZOOMLEVEL 1-22}" apikey="{APIKEY}" style="width:100%;height:400px;"></div>
<script>
function mapsInitiate() {
	var map = new google.maps.Map(document.getElementById('googleMapsMap'), {
		zoom: 10,
		center: new google.maps.LatLng(48.3114648, 11.918875800000023) // Erding
	});
	var marker = new google.maps.Marker({
		map: map,
		position: new google.maps.LatLng(48.30928, 11.908879999999954) // RossaMedia GmbH
	});
}
</script>
```
3. c) Google-Analytics
```js
var googleAnalyticsId = '{ANALYTICSID}';
(function googleAnalytics(){
	if(typeof window.mhCookie !== 'object') {
		setTimeout(googleAnalytics, 100);
	} else {
		if(window.mhCookie.check('googleAnalytics')) {
			// Global site tag (gtag.js) - Google Analytics
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = 'https://www.googletagmanager.com/gtag/js?id='+googleAnalyticsId;
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('set', 'anonymize_ip', true);
			gtag('config', googleAnalyticsId, { cookie_flags: 'secure;samesite=lax' });
		}
	}
})();
var disableStr = 'ga-disable-' + googleAnalyticsId;
if (document.cookie.indexOf(disableStr + '=true') > -1) {
	window[disableStr] = true;
}
function gaOptout() {
	document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	window[disableStr] = true;
	alert('Das Tracking durch Google Analytics wurde in Ihrem Browser für diese Website deaktiviert.');
}
```
