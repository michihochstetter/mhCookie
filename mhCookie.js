/*! mhCookie v1.4 | (c) Michael Hochstetter !*/
class mhCookie {
	constructor() {
		// Variables
		this.script = null;
		this.configName = 'mhCookie_config';
		this.config = null;
		this.tools = null;
		this.googleMapsMap = null;
		// Functions
		this.get = function(name) {
			var localStorageItem = window.localStorage.getItem(name);
			return localStorageItem !== null
				? JSON.parse(localStorageItem)
				: {};
		};
		this.set = function(name, value) {
			if (typeof value === 'object') value = JSON.stringify(value);
			window.localStorage.setItem(name, value);
		};
		this.check = function(tool = '') {
			if (tool != '') {
				var cookieConfig = this.get(this.configName);
				if (
					typeof cookieConfig[tool] !== 'undefined' &&
					cookieConfig[tool] === true
				) {
					return true;
				}
			}
			return false;
		};
		this.erase = function(name) {
			window.localStorage.removeItem(name);
		};
		// Google Maps
		this.generateMapsScript = function(map, ownCallback, callbackName) {
			this.googleMapsMap = map;

			if (typeof $(this.googleMapsMap).attr('callback') !== 'undefined') {
				ownCallback = true;
				callbackName = $(this.googleMapsMap).attr('callback');
			}

			var apikey = $(this.googleMapsMap).attr('apikey');
			if(apikey!='') {
				(g=>{var h,a,k,p='The Google Maps JavaScript API',c='google',l='importLibrary',q='__ib__',m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement('script'));e.set('libraries',[...r]+'');for(k in g)e.set(k.replace(/[A-Z]/g,t=>'_'+t[0].toLowerCase()),g[k]);e.set('callback',c+'.maps.'+q);a.src='https://maps.'+c+'apis.com/maps/api/js?'+e;d[q]=f;a.onerror=()=>h=n(Error(p+' could not load.'));a.nonce=m.querySelector('script[nonce]')?.nonce||'';m.head.append(a)}));d[l]?console.warn(p+' only loads once. Ignoring:',g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
					key: apikey,
				});
			}

			if(!ownCallback && typeof window.mhCookie.initGoogleMaps==='function') window.mhCookie.initGoogleMaps();
			else window[callbackName]();

			this.generateRevocation();
		};
		this.initGoogleMaps = function() {
			var latLngMarker = {
				lat: parseFloat($(this.googleMapsMap).attr('lat')),
				lng: parseFloat($(this.googleMapsMap).attr('lng')),
			};
			var map = new window.google.maps.Map(this.googleMapsMap, {
				center: latLngMarker,
				zoom:
					typeof $(this.googleMapsMap).attr('zoom') !== 'undefined'
						? parseInt($(this.googleMapsMap).attr('zoom'))
						: 10,
			});
			var marker = new window.google.maps.Marker({
				position: latLngMarker,
				map: map,
			});
		};
		this.generateRevocation = function(buttonText = '') {
			var siteLink = this.script.data('seite');
			$(
				"<br><a href='" +
					siteLink +
					"' class='mhCookie_btn' onClick='window.mhCookie.config[\"googleMaps\"]=false; window.mhCookie.set(\"" +
					this.configName +
					'", window.mhCookie.config); location.reload(); return false;\'>' +
					(buttonText != ''
						? buttonText
						: '🍪 Zustimmung widerrufen') +
					'</a>'
			).insertAfter(this.googleMapsMap);
		};
		this.generateError = function(element, tool, text, buttonText) {
			var siteLink = this.script.data('seite');
			$(
				"<div class='mhCookie_error'><p>" +
					text +
					"</p><p><a href='" +
					siteLink +
					"' class='mhCookie_btn' onClick='window.mhCookie.config[\"" +
					tool +
					'"]=true; window.mhCookie.set("' +
					this.configName +
					'", window.mhCookie.config); location.reload(); return false;\'>' +
					buttonText +
					'</a></p></div>'
			).insertBefore(element);
		};
		// Init
		this.init = function($) {
			var thisClass = this;
			this.script = $('#mhCookie_script');
			this.tools = this.script.data('tools').split(',');
			this.config = this.get(this.configName);
			var toolsHtml = '';
			this.tools.forEach((element) => {
				if (typeof this.config[element] === 'undefined')
					this.config[element] = false;
				switch (element) {
					case 'googleMaps':
						toolsHtml +=
							'<li><label><input type="checkbox" name="mhCookie_link_choose_'+element+'" class="mhCookie_link_choose_list_item_cbx" value="' +
							element +
							'"' +
							(this.config[element] === true ? ' checked' : '') +
							'><span>Google Maps</span></label></li>';
						if ($('[mhCookie-tool="' + element + '"]').length) {
							$('[mhCookie-tool="' + element + '"]').each(
								function() {
									if (
										thisClass.config[element] === true &&
										this.tagName.toLowerCase() !== 'iframe'
									) {
										thisClass.generateMapsScript(this);
									} else if (
										thisClass.config[element] === true &&
										this.tagName.toLowerCase() === 'iframe'
									) {
										$(this).attr(
											'src',
											$(this).data('src')
										);
										thisClass.googleMapsMap = $(this);
										thisClass.generateRevocation();
									} else {
										thisClass.generateError(
											this,
											element,
											'Für die Kartenansicht müssen Sie den Google Datenschschutzrichtlinien (<a href="https://www.google.com/policies/privacy/" target="_blank">https://www.google.com/policies/privacy/</a>) zustimmen und das Google Maps Cookie auf unserer Webseite erlauben.',
											'🍪 zustimmen'
										);
										this.remove();
									}
								}
							);
						}
						break;
					case 'googleAnalytics':
						toolsHtml +=
							'<li><label><input type="checkbox" name="mhCookie_link_choose_'+element+'" class="mhCookie_link_choose_list_item_cbx" value="' +
							element +
							'"' +
							(this.config[element] === true ? ' checked' : '') +
							'><span>Google Analytics</span></label></li>';
						break;
					case 'googleAdsense':
						toolsHtml +=
							'<li><label><input type="checkbox" name="mhCookie_link_choose_'+element+'" class="mhCookie_link_choose_list_item_cbx" value="' +
							element +
							'"' +
							(this.config[element] === true ? ' checked' : '') +
							'><span>Google Adsense</span></label></li>';
						break;
					case 'googleRecaptcha':
						toolsHtml +=
							'<li><label><input type="checkbox" name="mhCookie_link_choose_'+element+'" class="mhCookie_link_choose_list_item_cbx" value="' +
							element +
							'"' +
							(this.config[element] === true ? ' checked' : '') +
							'><span>Google ReCaptcha</span></label></li>';
						if ($('[mhCookie-tool="' + element + '"]').length) {
							$('[mhCookie-tool="' + element + '"]').each(
								function() {
									if (thisClass.config[element] === false) {
										thisClass.generateError(
											this,
											element,
											'Um diese Aktion durchzuführen, müssen Sie das Google ReCaptcha Cookie auf unserer Webseite erlauben.',
											'🍪 zustimmen'
										);
									}
								}
							);
						}
						break;
				}
			});
			var statusAll = null;
			var statusAllDiff = false;
			var statusAllTmp = null;
			Object.values(this.config).forEach((value) => {
				if (statusAllTmp !== null && statusAllTmp !== value) {
					statusAllDiff = true;
				}
				statusAllTmp = value;
			});
			if (!statusAllDiff) statusAll = statusAllTmp;
			var siteLink = this.script.data('seite');

			if ($('#mhCookie_container').length) {
				$('#mhCookie_container').remove();
			}
			$('body').append(
				'<div id="mhCookie_toggle" title="Cookie Einstellungen öffnen">🍪</div>'
			);
			$('body').append('<div id="mhCookie_container"></div>');
			$('#mhCookie_container').append('<div id="mhCookie_text"></div>');
			$('#mhCookie_text').append('<div id="mhCookie_headline">🍪 Cookies</div>');
			$('#mhCookie_text').append(
				'<p>Diese Webseite verwendet Cookies. Durch diese kann u.a. fest&shy;ge&shy;stellt werden, ob Sie uns eine Einwilligung zu der Verarbeitung der Sie betreffenden personenbezogenen Daten für einen oder mehrere bestimmte Zwecke gegeben haben (Art. 6 Abs. 1 lit a DSGVO).<br>In der Datenschutzerklärung finden Sie weitere Informationen zu den ein&shy;ge&shy;setz&shy;ten Cookies.</p>'
			);
			$('#mhCookie_text').append(
				'<a href="' + siteLink + '">&raquo; Datenschutzerklärung</a>'
			);
			$('#mhCookie_container').append('<div id="mhCookie_link"></div>');
			$('#mhCookie_link').append(
				'<a href="' +
					siteLink +
					'" id="mhCookie_link_btn_yes"' +
					(statusAll === true ? ' class="active"' : '') +
					'>allen zustimmen</a>'
			);
			$('#mhCookie_link').append(
				'<a href="" id="mhCookie_link_btn_no"' +
					(statusAll === false && statusAllDiff === false
						? ' class="active"'
						: '') +
					'>alle ablehnen</a>'
			);
			$('#mhCookie_link').append(
				'<a href="" id="mhCookie_link_btn_choose"' +
					(statusAllDiff === true ? ' class="active"' : '') +
					'>auswählen</a>'
			);
			$('#mhCookie_link').append(
				'<div id="mhCookie_link_choose_list">' +
					'<ul>' +
						'<li>✅ funktionale Cookies</li>' +
						(toolsHtml!='' ? toolsHtml : '') +
					'</ul>' +
				'</div>'
			);
			$('rm#mhCookie_datenschutz').append(
				'<button id="mhCookie_datenschutz_toggle">🍪 Cookie Einstellungen öffnen</button>'
			);
			$('#mhCookie_link_btn_yes').on('click', function(e) {
				e.preventDefault();
				$('#mhCookie_link a').removeClass('active');
				$(this).addClass('active');
				thisClass.tools.forEach(
					(element) => (thisClass.config[element] = true)
				);
				thisClass.set(thisClass.configName, thisClass.config);
				location.reload();
			});
			$('#mhCookie_link_btn_no').on('click', function(e) {
				e.preventDefault();
				$('#mhCookie_link a').removeClass('active');
				$(this).addClass('active');
				thisClass.tools.forEach(
					(element) => (thisClass.config[element] = false)
				);
				thisClass.set(thisClass.configName, thisClass.config);
				$('#mhCookie_container').fadeOut('fast');
				$('#mhCookie_toggle').fadeIn('fast');
			});
			$('#mhCookie_link_btn_choose').on('click', function(e) {
				e.preventDefault();
				$('#mhCookie_link a').removeClass('active');
				$(this).addClass('active');
				$('#mhCookie_link_choose_list').slideToggle('fast');
			});
			$('.mhCookie_link_choose_list_item_cbx').on('change', function(e) {
				e.preventDefault();
				var tool = $(this).val();
				var checked = $(this).prop('checked');
				thisClass.config[tool] = checked;
				thisClass.set(thisClass.configName, thisClass.config);
			});
			$('#mhCookie_datenschutz_toggle, #mhCookie_toggle').on(
				'click',
				function() {
					$('#mhCookie_toggle').hide();
					$('#mhCookie_container').fadeIn('fast');
				}
			);
			if (Object.keys(this.get(this.configName)).length === 0) {
				setTimeout(function() {
					$('#mhCookie_container').fadeIn('fast');
				}, 1500);
			} else {
				$('#mhCookie_toggle').fadeIn('fast');
			}
		};
	}
}

// Init
var mhC = new mhCookie();
document.addEventListener('readystatechange', function(event) {
	if (event.target.readyState === 'interactive') {
		window.mhCookie = mhC;
	} else if (event.target.readyState === 'complete') {
		if (typeof window.mhCookie !== 'object') {
			window.mhCookie = mhC;
		}
		window.mhCookie.init(jQuery || window.jQuery);
	}
});

(function mhCookieScroll() {
	if (!window.jQuery) {
		setTimeout(mhCookieScroll, 100);
	} else {
		var lastScrollTop = 0;
		window.addEventListener(
			'scroll',
			function(event) {
				var st =
					window.pageYOffset || document.documentElement.scrollTop;
				if (st > lastScrollTop) {
					if (jQuery('#mhCookie_toggle').css('right') == '24px') {
						jQuery('#mhCookie_toggle').animate(
							{
								right: '-48px',
							},
							250
						);
					}
				} else {
					if (
						jQuery('#mhCookie_toggle').css('right') == '-48px' &&
						jQuery('#mhCookie_toggle').is(':visible')
					) {
						jQuery('#mhCookie_toggle').animate(
							{
								right: '24px',
							},
							250
						);
					}
				}
				lastScrollTop = st <= 0 ? 0 : st;
			},
			false
		);
	}
})();